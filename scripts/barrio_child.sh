#!/bin/bash

if [ -z "$1" ]; then
        echo "devi passare il nome del tema"
	exit 1;
fi

export SUBTHEME=$1
mkdir -p web/themes/custom/$SUBTHEME
cd web/themes/custom
cp -r ../contrib/bootstrap_barrio/subtheme/* $SUBTHEME
cd $SUBTHEME
for file in *bootstrap_barrio_subtheme.*; do mv $file ${file//bootstrap_barrio_subtheme/$SUBTHEME}; done
for file in config/*/*bootstrap_barrio_subtheme.*; do mv $file ${file//bootstrap_barrio_subtheme/$SUBTHEME}; done
mv {_,}$SUBTHEME.theme
#echo mv {_,}$SUBTHEME.layouts.yml
grep -Rl bootstrap_barrio_subtheme .|xargs -I '{}' sed -i '{}' -e "s/bootstrap_barrio_subtheme/$SUBTHEME/"
