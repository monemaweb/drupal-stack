# PHP MySQL Nginx 

Docker compose per tirare su uno stack PHP/MySQL/Nginx con:

* PHP 8.0
* MySQL 8.0
* Nginx Ultima versione
* Mailhog

## Setup variabili d'ambiente

Copiare il file .env.example in .env e modificare i dati per l'accesso al database

## Configurazione Nginx

Modificare il file config/nginx/nginx.conf

## Creare i container

eseguire il comando:


```
docker-compose up -d
```


## Installare DRUPAL con composer

Per installare la prima volta Drupal è possibile utilizzare l'immagine docker 'composer'


```
docker run --rm -i --tty \
   -v $PWD/www:/app \
   composer \
   create-project drupal/recommended-project . --ignore-platform-reqs
docker run --rm -i --tty \
   -v $PWD/www:/app \
   composer \
   install
```

## Installare il tema boostrap


```
cp ./scripts/barrio_child.sh www
docker run --rm -i --tty \
  -v $PWD/www:/var/www/html \
  php-fpm-8 \
  composer require 'drupal/bootstrap_barrio:^5.5'
docker run --rm -i --tty \
  -v $PWD/www:/var/www/html \
  php-fpm-8 \
  ./barrio_child.sh dfg
```


## Install drush


```
docker run --rm -i --tty \
   -v $PWD/www:/var/www/html \
   php-fpm-8 \
   composer require --dev drush/drush
```

## Accesso

Il webserver è configurato per rispondere sulla porta 9123, quindi accedere alla seguente url:

http://localhost:9123/

Mailhog è accedibile all'url

http://localhost:9123/__mailhog


